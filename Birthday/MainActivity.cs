﻿using System;
using Android.App;
using Android.Widget;
using Android.OS;

namespace Birthday
{
    [Activity(Label = "Birthday", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity
    {
        int count = 1;
        int birthDay = 13;
        int birthMounth = 12;
        int birthYear = 2010;
        TimeSpan timeTo;
        TextView textView;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Main);

            Button button = FindViewById<Button>(Resource.Id.myButton);
            textView = FindViewById<TextView>(Resource.Id.daysLast);

            DateTime dateNow = DateTime.Now;
            int thisDay = dateNow.Day;
            int thisMonth = dateNow.Month;
            int thisYear = dateNow.Year;

            if (thisMonth < birthMounth){
                DateTime birthday = new DateTime(thisYear, birthMounth, birthDay);
                timeTo = birthday.Subtract(dateNow);
            }
            else{

            }

            button.Click += delegate {
                setTextDays();
            };
        }

        public void setTextDays()
        {
            textView.Text = timeTo.ToString("d' днів 'h' годин 'm' хвилин'");
            textView.SetTextSize(Android.Util.ComplexUnitType.Dip, 30);
            textView.SetTextColor(Android.Graphics.Color.Green);
        }
    }
}

